#
#===============================================================================
#
#         FILE: Koha_MARC2RDF.t
#
#  DESCRIPTION: Test file
#
#        FILES: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Chris Cormack (rangi), chrisc@catalyst.net.nz
# ORGANIZATION: Koha Development Team
#      VERSION: 1.0
#      CREATED: 14/08/13 09:49:06
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
use MARC::Record;
use Test::RDF;

use Test::More tests => 7;    # last test to print

use_ok('Koha::MARC2RDF');

my $uribase = 'http://test.test/';
ok( my $rdf       = Koha::MARC2RDF->new($uribase) );
ok( my $namespace = $rdf->namespace() );

is( $namespace->{'OV'}, 'http://open.vocab.org/terms', 'Testing namespace' );

my $marcrecord = MARC::Record->new;

$marcrecord->add_fields(
    [ '001', '1234' ],
    [ '245', ' ', ' ', a => 'Cooking RDF' ],
    [ '110', ' ', ' ', a => 'Chris Cormack' ],
    [ '650', ' ', ' ', a => 'Cooking' ],
    [ '650', ' ', ' ', a => 'Cookery', z => 'Instructional manuals' ],
);

ok( my $converted = $rdf->convert($marcrecord) );
like( $converted, qr/Koha_1234/, 'Testing rdf' );
is_valid_rdf($converted,'NTriples');
print $converted;

